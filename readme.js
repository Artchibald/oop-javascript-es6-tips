// we're organising code
// OOP - the hard parts
// Organise code
// name, score are not properties in objects
// we never parse into functions and they appear in properties.
// They are put in local memory and then put in an objects
// Always remember the difference.
// Everything in stored in the "function object combo"
// Great software engineer: knowledge is always wevolving and changing - how capable are you with new knowledge.
// Diagraming

// 5 capacities

// 1. Analytical problem solving
// 2. Tech com
// 3. Engineering best practices and approach
// 4. Non tehcnical commonunication (leyman terms)
// 5. Computer science experience

// Expectations:
// -Support each other
// -Work hard and smart
// OOP
// why do we organise?
// 1
// do this
// 2 do that
// ...
// ORGANISE IT!
// A quiz:
// the players
// the questions
// the score
// =Display data!
// Let's combine up data with functionality
// We bundle display question in questions
// -easy to add features
// -easy to undertsand
// -performant
// Paradigm of code = organised codw
// how to bundle
// === PUT IT IN A OBJECT!
// Raw data:

// Name:Phil
// Score: 4
// Name:Julia
// Score: 2

// In a object - store functions and associated data

const user1 = {
    name: Phil,
    score: 4,
    increment: function () {
        user1.score++;
    }
};
user1.increment();

// this is encapsulation
// its right there on the object itself
//its strict
//bundle in object

//other object approach

//assign proerties with . notation

const user2 = {};//empty object, we can't change this object, we can change its properties, it has an "empty object literal" assigned 
user2.name = "Julia";//assign properties to that object, assign a name property to the object, with value julia
user2.score = 5;//assign properties to that object, assign a name property called score to the object, with value 5
//can we then increment the property of the function? YES
user2.increment = function () {
    user2.score++;
}

//What might our object have access to? object.create: Whatever we pass to that, it always returns out an empty object
const user3 = Object.create(null);
user3.name = "Eva";
user3.score = 6;
user3.increment = function () {
    user3.score++;
};

// All 3 are the same thing!

// We wrap all the actions in functions to keep it clean: we wrap all 3 users into 1

function userCreator(name, score) {
    const newUser = {};
    newUser.name = name;
    newUser.score = scroe;
    newUser.increment = function () {
        newUser.score++;
    };
    return newUser;
};
const user1 = userCreator("Phil", 4);
const user2 = userCreator("Julia", 6);
user1.increment();


//we are doing copies of every incremental functions which is not viable 
//if we have 100 users its copies of copies, not good
//data is unique, but functions are just same copies

//store our functions we want our user to have access to in a new object

//the PROTOTYPE CHAIN
const functionStore = {
    increment: function () { this.score++; },
    login: function () { console.log("you're logged in") }
};

const user1 = {
    name: "phil",
    score: 4
}

user1.name // name is a propertyt of user1 object
user1.increment // Error! increment is not!

//below is current best approach

function userCreator(name, score) { //declare new function
    const newUser = Object.create(userFunctionStore);
    newUser.name = name;// parameter
    newUser.score = score;// parameter
    return newUser;//envoke
};

const userFunctionStore = {//declare object with properties which are functions
    increment: function () { this.score++; },
    login: function () { console.log("You're loggedin"); }
};
const user1 = userCreator("Phil", 4);//new const/player added thanks to user creator,  
const user2 = userCreator("Julia", 6);
user1.increment();

//there are hidden objects in properties, bonus functions
//light grey properties in inspector
// these are prototypes
// prototyping means if JS cant find the object or function, it doesnt give up immediately, it looks elsewhere for it 
//this is a perfect solution that uses Javascripts PROTOTYPAL NATURE
//js then looks at the PROTO property not prototype
// the keyword "new"
// we need "this"
//where can we put our single copies of the functions
function multiplyBy2(num) {
    return num * 2
}//as soon as this is delared its a function but its also an object
multiplyBy2.stored = 5;//5
multiplyBy2(3); //6
multiplyBy2.stored;//5
multiplyBy2.prototype; //6

//we could use the fact that all functions have a defaultproperty on theirnobject version, "prototype" which isitselfan object- to replace our functionStore object
//PUZZLING:functions are both objects and functions
// prototype is a property of the object
function UserCreator(name, score) { //declare new function
    this.name = name;// parameter
    this.score = score;// parameter
}
UserCreator.prototype.increment = function () {
    this.score++;
}
UserCreator.prototype.login = function () {
    console.log("login");
}
const user1 = new UserCreator("Eva", 9)

user1.increment()
user1.login()
console.log(user1.name + " and " + user1.score)

//the new keyword adds a bunch of properties
//new creates:
//1. new empty object
//__proto__ : hidden property,reference to other object
//welook in the __proto__ which is a link to the propotype object on the userCreator object

//this last solution is fast to write, encapsulated

console.dir(user1);
//the above maps the tree ofproperties
//take UserCreator it has "this" inside, it has a uppercase first letter, that is a hint to other developers that this requires new to work!
//remember all functions are function - object combos

//this is called an implicit parameter

//__proto autoknows to check prototype
//lexical static scoping, a function with 100 lines
// it gets deleted when leave execution context
//"where I was born, whereni was saved,determins something when i get called."
// the ARROW function lets it know that the prototype is the same as the object now

function UserCreator(name, score) { //declare new function
    this.name = name;// parameter
    this.score = score;// parameter
}
UserCreator.prototype.increment = function () {
    const add1 = () => { this.score++ }
    add1()
}
UserCreator.prototype.login = function () {
    console.log("login");
}
const user1 = new UserCreator("Eva", 9)

user1.increment()

//this is known as a javascript class
//this is addition of es6 shorthand:
class UserCreator { //declare new function
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
    increment() {
        this.score++;
    }
    login() {
        console.log("login");
    }
}
const user1 = new UserCreator("Eva", 9)

user1.increment();
console.dir(user1);

//new example
//btw an array is definitely also an object therefore also a function
//we declare a new obj
//js wont find the new property, its in__proto__ that links to prototype (shared fucntions, now we ref prototype)
const obj {
    num:3
}
obj.num//3
obj.hasOwnProperty("num")//where is this method, its a string....
Object.prototype//{hasOwnProtperty:FUNCTION}


//arrays and functions are also objects so they get access to all the functions in Object.prototype but also more goodies

function multiplyBy2(num) {
    return num * 2
}
multiplyBy2.toString()//where is this method
Function.prototype//{toString : FUNCTION, call: FUNCTION bind: FUnciton}
multiplyBy2.hasOwnProperty("score")//whieres this functiooN?
Function.prototype.__proto__//Object.prototype{hasOwnProperty:FUNCTION}
// with this set up we can organise what has access to waht with shared functions in a organised way

//SUBCLASSING : passing knowledger down the tree
//we have different users, paid and free some have access to properties that some dont
//<img src="subclassing">

//Creating an object with a sub-factory Fuction
function userCreator(name, score) {
    const newUser = Object.create(userFunctions);
    newUser.name = name;
    newUser.score = score;
    return newUser;
}

userFunctions = {
    sayName: function () {
        console.log("I'm " + this.name);
    },
    increment: function () {
        this.score++;
    }
}
const user1 = userCreator("Phil", 5);
user1.sayName();


function paidUserCreator(paidName, paidScore, accountBalance) {
    const newPaidUser = userCreator(paidName, paidScore);
    Object.setPrototypeOf(newPaidUser, paidUserFunctions);
    newPaidUser.accountBalance = accountBalance;
    return newPaidUser;
}
const paidUserFunctions = {
    increaseBalance: function () {
        this.accountBalance++;
    }
};
Object.setPrototypeOf(paidUserFunctions, userFunctions)
const paidUser1 = paidUserCreator("Alyssa", 8, 25);
paidUser1.increaseBalance();
paidUser1.sayName();

console.dir(user1);


//interlude, we need this fornext script:
//we have another way of runniong a function that allows us to control the assignement of this
const obj = {
    num: 3;
    increment: funcyion(){ this.num++}
};
const otherObj = {
    num: 10
};
obj.increment();
obj.increment.call(otherObj);

//this always refers tothe object to the left of the dot on which the function(methid)is being called. Unless we override that by running the function using .call() or apply() which lets us ser t the value of this insde the increment function

//subclass review
//extends keyword

//subclassing(constructor pseudoclassical approach)

//the hardest part of javascript

function userCreator(name, score) {
    this.name = name;
    this.score = score;
}
userCreator.prototype.sayName = function () {
    console.log("I'm " + this.name);
}
userCreator.prototype.increment = function () {
    this.score++;
}
const user1 = new userCreator("Phil", 5);
const user2 = new userCreator("Tim", 9);
user1.sayName;

function paidUserCreator(paidName, paidScore, accountBalance) {
    userCreator.call(this, paidName, paidScore);
    this.accountBalance = accountBalance;
}
paidUserCreator.prototype = Object.create(userCreator.prototype);
paidUserCreator.prototype.increaseBalance = function () {
    this.accountBalance++;
};
const paidUser1 = new paidUserCreator("Alyssa", 8, 25);
paidUser1.increaseBalance()
paidUser1.sayName()
//now we have things only paind users can access users cant. 
console.dir(paidUser1);

// Now the very last step of OOP

//now we can replicate OOP in prototypaljs env!

//create an object with a class
//es15 class approach
class userCreator {
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
    sayName() {
        console.log("I am " + this.name);
    }
    increment() {
        this.score++;
    }
}
const user1 = new userCreator("Phil", 4);
const user2 = new userCreator("TIm", 5);

user1.sayName();

class paidUserCreator extends userCreator {
    constructor(paidName, paidScore, accountBalance) {
        super(paidName, paidScore);
        this.accountBalance = accountBalance;
    }
    increaseBalance() {
        this.accountBalance++;
    }
}
const paidUser1 = new paidUserCreator("Allyssa", 8, 25);
paidUser1.increaseBalance();
paidUser1.sayName();
console.dir(paidUser1);


//FINAL ONE LIKE REACT
class userCreator {
    constructor(name, score) {
        this.name = name;
        this.score = score;
    }
    sayName() {
        console.log("I am " + this.name);
    }
    increment() {
        this.score++;
    }
}
const user1 = new userCreator("Phil", 4);
const user2 = new userCreator("TIm", 5);

user1.sayName();

class paidUserCreator extends userCreator {
    constructor(paidName, paidScore, accountBalance) {
        super(paidName, paidScore);
        this.accountBalance = accountBalance;
    }
    increaseBalance() {
        this.accountBalance++;
    }
}
const paidUser1 = new paidUserCreator("Alyssa", 5, 10);

paidUser1.increaseBalance();
paidUser1.sayName();

console.dir(paidUser1);